<?php

// require_once('src/Model/DBModel.php');
require_once('Model/DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'root',			//	Changed from default
                '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals($bookList[0]->id, 1);
        $this->assertEquals($bookList[0]->title, 'Jungle Book');
        $this->assertEquals($bookList[1]->id, 2);
        $this->assertEquals($bookList[1]->author, 'J. Walker');
        $this->assertEquals($bookList[2]->id, 3);
        $this->assertEquals($bookList[2]->description, 'Written by some smart gal.');
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book.');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"

    /////       Test Case 1
    public function testAddBook()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    /////       Test Case 2
    public function testAddBookWithoutDescription()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => ''];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
            
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
            
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    /////       Test Case 3
    public function testAddBookEscapingSpecialChars()
    {
        $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                       'author' => "<script>document.body.style.visibility='hidden'</script>",
                       'description' => "<script>document.body.style.visibility='hidden'</script>"];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
            
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
            
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
		$testValues = ['title' => 'New Book',
					   'author' => 'Some author',
					   'description' => 'Some description',
					   'id' => 'someId'];
		//	Assign it to some random ID that is not a number
		$book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);

        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        }
        catch(InvalidArgumentException $ex){ }
        
        $this->tester->seeNumRecords(3, 'book');
    }

    // Tests that adding a book fails if mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
        $testValues = ['title' => 'New Book',
                       'author' => null,
                       'description' => 'Some description'];
        
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
		
        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        }
        catch(InvalidArgumentException $ex){ }
                       
        $this->tester->seeNumRecords(3, 'book');
    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"

    /////       Test Case 1
    public function testModifyBook()
    {
		$testValues = ['title' => 'New book',
					   'author' => 'Some author',
                       'description' => 'Some description',
                       'id' => 1];
		//	Let's test book with ID 1
		$book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
		$this->dbModel->modifyBook($book);
		
        $this->tester->seeInDatabase('book', ['id' => $testValues['id'],
                                     'title' => $testValues['title'],
                                     'author' => $testValues['author'],
                                     'description' => $testValues['description']]);
    }

    /////       Test Case 2
    public function testModifyBookWithoutDescription()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description',
                       'id' => 1];
        //	Let's test book with ID 1
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
        $this->dbModel->modifyBook($book);
            
        $this->tester->seeInDatabase('book', ['id' => $testValues['id'],
                                      'title' => $testValues['title'],
                                      'author' => $testValues['author'],
                                      'description' => $testValues['description']]);
    }

    /////       Test Case 2
    public function testModifyEscapingSpecialChars()
    {
        $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                       'author' => "<script>document.body.style.visibility='hidden'</script>",
                       'description' => "<script>document.body.style.visibility='hidden'</script>",
                       'id' => 1];
        //	Let's test book with ID 1
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
        $this->dbModel->modifyBook($book);
            
        $this->tester->seeInDatabase('book', ['id' => $testValues['id'],
                                      'title' => $testValues['title'],
                                      'author' => $testValues['author'],
                                      'description' => $testValues['description']]);
    }


    
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {       
        $testValues = ['title' => 'Some book',
                       'author' => 'Some author',
                       'description' => 'Some description',
                       'id' => 'four'];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);

        try{
            $this->dbModel->modifyBook($book);            
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        }
        catch(InvalidArgumentException $ex){ } 
    }
    
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {       
        $testValues = ['title' => 'Some title',
                       'author' => '',
                       'description' => 'Some description',
                       'id' => 1];
        
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);

        try{
            $this->dbModel->modifyBook($book);            
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        }
        catch(InvalidArgumentException $ex){ } 
    }
    
    // Tests that a book record can be successfully removed.
    public function testDeleteBook()
    {
        $id = 2;

        $book = $this->dbModel->getBookById($id);           //  Get a copy of the book
        $this->dbModel->deleteBook($id);  

        $this->tester->seeNumRecords(2, 'book');

        //  Then check if such a book still exists in our database
        $this->tester->dontSeeInDatabase('book', ['id' => $book->id,
                                                  'title' => $book->title,
                                                  'author' => $book->author,
                                                  'description' => $book->description]);
    }
    
    // Tests that removing a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
        $id = "two";

        try{
            $this->dbModel->deleteBook($id);          
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        }
        catch(InvalidArgumentException $ex){ } 

        $this->tester->seeNumRecords(3, 'book');
    }
}